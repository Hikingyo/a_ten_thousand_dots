/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package betomorrowsquare;

/**
 *
 * @author hikingyo
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BetomorrowSquare {

    static Scanner in = new Scanner(System.in);
    static PrintStream out = System.out;
    static HashMap<Integer, HashMap<Integer, Boolean>> listePoints = new HashMap<>();
    static ArrayList<String> contenuFichier = new ArrayList<>();
    static Integer maxX = new Integer("0");
    static Integer maxY = new Integer("0");
    static Integer minX = new Integer("0");
    static Integer minY = new Integer("0");
    static double longueurMaxCarrePow2;
    static int nbCarres = 0;

    
    public static void main(String[] args) {
        //Lecture fichier
        contenuFichier = lectureFichier();
        /**
         * Structuration des données
         */
        
        // Pour chaque ligne 
        for (String ligne : contenuFichier) {
            //On nettoie la ligne et on sépare abscisse et ordonnée
            String[] coordonnees = ligne.trim().split(" ");

            Integer px = new Integer(coordonnees[0]);

            // On recherche les extremum
            if (px > maxX) {
                maxX = px;
            }
            if (px < minX) {
                minX = px;
            }

            Integer py = new Integer(coordonnees[1]);

            if (py > maxY) {
                maxY = py;
            }
            if (py < minY) {
                minY = py;
            }
            // On stock les coordonnées dans un tableau bidimensionnel
            if (!listePoints.containsKey(px)) {// Il n'y aucun point avec la même abscisse
                // On crée le tableau correspondant aux ordonnées
                HashMap<Integer, Boolean> axeY = new HashMap<>();
                // On stocke le point
                axeY.put(py, Boolean.TRUE);
                listePoints.put(px, axeY);
            } else {//Sinon le tableau d'ordonnées existe déjà
                HashMap axeY = listePoints.get(px);
                if (!axeY.containsKey(py)) {//On vérifie les doublons
                    // On stocke le point
                    axeY.put(py, Boolean.TRUE);
                    listePoints.put(px, axeY);
                }
            }
        }

        // On détermine les dimesions maximales d'un carré
        int largeurChamps = Math.abs(maxX) + Math.abs(minX);
        int hauteurChamps = Math.abs(maxY) + Math.abs(minY);
        // On calcule la dimension max au carré pour éviter un calcul de racine
        // par la suite.
        if (largeurChamps > hauteurChamps) {
            longueurMaxCarrePow2 = largeurChamps * largeurChamps;
        } else {
            longueurMaxCarrePow2 = hauteurChamps * hauteurChamps;
        }
        
        /**
         * Recherche des carrés
         */
        
        // Parcour des abscisses
        Iterator<Map.Entry<Integer, HashMap<Integer, Boolean>>> itx;
        itx = listePoints.entrySet().iterator();
        while (itx.hasNext()) {
            Map.Entry<Integer, HashMap<Integer, Boolean>> listeX = itx.next();
            Integer aX = listeX.getKey();
            HashMap<Integer, Boolean> axeY = listeX.getValue();
            
            // Parcours des ordonnées
            Iterator<Map.Entry<Integer, Boolean>> ity;
            ity = axeY.entrySet().iterator();
            // Pour chaque point
            while (ity.hasNext()) {
                Map.Entry<Integer, Boolean> listeY = ity.next();
                Integer aY = listeY.getKey();
                // On cherche un premier segment
                Iterator<Map.Entry<Integer, HashMap<Integer, Boolean>>> itx2;
                itx2 = listePoints.entrySet().iterator();
                while (itx2.hasNext()) {
                    Map.Entry<Integer, HashMap<Integer, Boolean>> listeX2 = itx2.next();
                    Integer bX = listeX2.getKey();
                    HashMap<Integer, Boolean> axeY2 = listeX2.getValue();

                    Iterator<Map.Entry<Integer, Boolean>> ity2;
                    ity2 = axeY2.entrySet().iterator();
                    while (ity2.hasNext()) {
                        Map.Entry<Integer, Boolean> listeY2 = ity2.next();
                        Integer bY = listeY2.getKey();
                        // Calcule de la longueur du segment au carré
                        int longueurSegmentPow2 = (aX - bX) * (aX - bX) + (aY - bY) * (aY - bY);
                        // Si le segment est valide
                        if (longueurSegmentPow2 <= longueurMaxCarrePow2 && longueurSegmentPow2 != 0) {
                            // On cherche un troisième sommet 
                            int cX = bX - (bY - aY);
                            if (listePoints.containsKey(cX)) {
                                int cY = bY + (bX - aX);
                                if (listePoints.get(cX).containsKey(cY)) {// Si le troisième sommet existe
                                    // On cherche le quatrième sommet correspondant
                                    int dX = aX - (bY - aY);
                                    if (listePoints.containsKey(dX)) {
                                        int dY = aY + (bX - aX);
                                        if (listePoints.get(dX).containsKey(dY)) {
                                            // Les 4 sommets sont référencés dans le tableau
                                            nbCarres++;
                                        }
                                    }
                                }
                            }
                            // On cherche un autres troisième sommet possible
                            cX = bX + (bY - aY);
                            if (listePoints.containsKey(cX)) {
                                int cY = bY - (bX - aX);
                                if (listePoints.get(cX).containsKey(cY)) {
                                    // On cherche le quatrième sommet correspondant
                                    int dX = aX + (bY - aY);
                                    if (listePoints.containsKey(dX)) {
                                        int dY = aY - (bX - aX);
                                        if (listePoints.get(dX).containsKey(dY)) {
                                            // Les 4 sommets sont référencés dans le tableau
                                            nbCarres++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // On supprime le point de départ du tableau
                ity.remove();
            }
            // Le tableau d'ordonnées étant vide, on le supprime
            itx.remove();
        }
        // Affichage
        out.println("Nombres de carré trouvés :" + nbCarres);
    }

    /**
     * Fonction de lecture du fichier
     *
     * @return contenu : la tableau de lignes du fichier
     */
    public static ArrayList<String> lectureFichier() {
        ArrayList<String> data = new ArrayList<>();
        Scanner readerFile;
        File fichierCoordonnees = new File("exercice6.txt");
        try {
            readerFile = new Scanner(fichierCoordonnees);
            while (readerFile.hasNextLine()) {
                data.add(readerFile.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BetomorrowSquare.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

}
